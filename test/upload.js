var request = require('supertest');
var express = require('express');
var fs = require('fs');
var expect = require('chai').expect;

var blobPath = __dirname + '/uploads/';

var app = require('./app');
var passportStub = require('passport-stub');

describe('POST /upload', function() {

  before(function() {
    passportStub.install(app);
    passportStub.login({username: 'john.doe'});
  });

  afterEach(function() {
    var files = fs.readdirSync(blobPath);
    files.forEach(function (file) {
      console.log('Deleting files ' + file);
      fs.unlinkSync(blobPath + '/' + file);
    });
  });

  it('ファイルをアップロードできること', function(done) {
    request(app)
      .post('/upload')
      .type('form')
      .attach('files', __dirname + '/files/test.jpg')
      .expect(302)
      .expect('location', '/')
      .end(function(err, res){
        if (err) {
          console.error('Error ' + err);
          throw err;
        };
        var uploadedFiles = fs.readdirSync(blobPath);
        expect(uploadedFiles).length(2);
        done();
      });
  });

});
