var React = require('react');
var ReactCanvas = require('react-canvas');
var request = require('request');
var Promise = require('bluebird');

var Surface = ReactCanvas.Surface;
var Image = ReactCanvas.Image;

module.exports = React.createClass({
  propTypes: {
    _id: React.PropTypes.string.isRequired,
    name: React.PropTypes.string.isRequired,
    originalname: React.PropTypes.string.isRequired,
    createdat: React.PropTypes.string.isRequired,
    width: React.PropTypes.number.isRequired,
    height: React.PropTypes.number.isRequired,
    author: React.PropTypes.object.isRequired,
    starCount: React.PropTypes.number.isRequired,
    isStared: React.PropTypes.bool.isRequired,
    onZoom: React.PropTypes.func.isRequired,
    handleOnStar: React.PropTypes.func.isRequired
  },

  getInitialState: function() {
    return {
      isStared: this.props.isStared,
      starCount: this.props.starCount
    };
  },

  componentWillReceiveProps: function(nextProps) {
    this.setState({
      isStared: nextProps.isStared,
      starCount: nextProps.starCount
    });
  },

  handleOnStar: function() {
    updateStar(this.props._id, this.state.isStared)
      .then(function() {
        var starCount = this.state.starCount + 1;
        if(this.state.isStared) {
          starCount = this.state.starCount - 1;
        }
        this.setState({
          isStared: !this.state.isStared,
          starCount: starCount
        });
        this.props.handleOnStar(this.props, this.state.starCount);
      }.bind(this));
  },

  _onZoom: function() {
    var photo = this.props;
    photo.starCount = this.state.starCount;
    this.props.onZoom(photo);
  },

  render: function() {
    var surfaceWidth = getWidth();
    var surfaceHeight = getWidth();
    var imageStyle = getImageStyle();
    var imageSrc = '/img/uploads/th/' + this.props.name;

    var starImg = '/img/icon-unstar.png';
    if(this.state.isStared) {
      starImg = '/img/icon-star.png';
    }

    return (
        <div className="thumb" >
        <div className="cursor" onClick={this._onZoom} >
        <Surface width={surfaceWidth} height={surfaceHeight} left={0} top={0}>
        <Image style={imageStyle} src={imageSrc} />
        </Surface>
        </div>
        <div className="thumb-meta">
        <div className="thumb-meta-inf photo-comment">{this.state.starCount} </div>
        <img className="thumb-meta-inf icon" src={starImg} onClick={this.handleOnStar} />
        </div>
        </div>
    );
  }
});

function getWidth() {
  if(window.innerWidth >= 480) {
    return Math.round(window.innerWidth * 18 / 100);
  } else {
    return Math.round(window.innerWidth * 32 / 100);
  }
}

function getImageStyle() {
  return {
    top: 0,
    left: 0,
    width: getWidth(),
    height: getWidth()
  };
};

function updateStar(id, isStared) {
  var path = '/photos/star/countup';
  if(isStared) {
    path = '/photos/star/countdown';
  }
  return new Promise(function(onFulfilled, onRejected) {
    request.put({
      uri: window.location.origin + path + '?id=' + id
    }, function(error, res, body) {
      if(error) {
        return onRejected(error);
      }
      return onFulfilled(body);
    });
  });
}
