party-photo-share(仮)
======================

[![wercker status](https://app.wercker.com/status/96d44a2c4e815cdbf511f0c314d4e212/s "wercker status")](https://app.wercker.com/project/bykey/96d44a2c4e815cdbf511f0c314d4e212)

Realtime photo sharing service at the party.

## Development

### Requirement

* Node.js >= 4.2

### Run with gulp

```bash
$ git clone git@bitbucket.org:mid0111/party-photo-share.git && cd party-photo-share
$ npm install && bower install

$ cp config/auth.sample.js config/auth.js
$ vi config/auth.js

$ mkdir -p build/img/uploads/{th,m,org}

$ docker run -p 27017:27017 --name mongodb -d mongo
$ ./node_modules/gulp/bin/gulp.js
```

### Optional

#### Run MongoDB Client

```bash
$ docker run -it --rm --link mongodb:mongodb mongo bash -c 'mongo --host mongodb'
```

#### Run with Nginx

To run this application throw Nginx.

```bash
$ docker build -t nginx-auth ./resources/nginx/
$ gulp build && docker run --name node -d -p 3000:3000 -v `pwd`:/data node node ./app.js
$ docker run --name nginx -it --rm -p 80:80 -v `pwd`/resources/nginx/conf.d:/etc/nginx/conf.d:ro -v `pwd`/build:/var/www/html --link node:nodejs nginx-auth /bin/bash
```

### Test

```bash
$ gulp test
```

## Deployment to AWS EC2

### Create AWS EC2 instance in advance of provisioning.  

* Create inventory file for ansible and specify target hostname.
```bash
$ cp resources/ansible/hosts.sample resources/ansible/hosts
$ vi resources/ansible/hosts
```

### Change configuration for OAuth.

```bash
$ cp config/auth.sample.js config/auth.js
$ vi config/auth.js
```

### Deploy and start server

```bash
$ docker run -it --rm --name ansible -v `pwd`/resources/ansible:/tmp/ansible -v `pwd`/config:/tmp/secret -v {path_for_pem_file}:/root/pem ansible/ubuntu14.04-ansible:stable ansible-playbook /tmp/ansible/site.yml --private-key="/root/pem/{pem_file_name}" -i /tmp/ansible/hosts
```

### Browse

* Open http://{EC2_instance_public_DNS} for user screen.
* Open http://{EC2_instance_public_DNS}/slides for slide show screen.


## License

[The MIT License](http://opensource.org/licenses/MIT)

Copyright (c) 2015 mid0111 < https://github.com/mid0111 >
